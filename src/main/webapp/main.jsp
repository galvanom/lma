<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<form action="" method="POST">
    <p><input type="submit" value="New board"></p>
</form>
<table>
  <c:forEach items="${boards}" var="boardId">
    <tr>
      <td><c:out value="Board id: ${boardId} "/>
      <a href="/board/watch?boardId=${boardId}">watch </a>
      or
      <a href="/board/draw?boardId=${boardId}"> draw</a>
      </td>
    </tr>
  </c:forEach>
</table>
</html>