<!DOCTYPE html>
<html>
<canvas height=400 width=600 id="board"></canvas>
<script type="text/javascript">
    var board = document.getElementById("board");
	var context = board.getContext("2d");
	context.strokeStyle = "#ffffff";

    loadFromServer();

	function loadFromServer(){
       var xhr = new XMLHttpRequest();
       xhr.open('GET', '/board/api/<%= request.getParameter("boardId") %>', false);
       xhr.send();
       var jsonText = xhr.responseText;
       if (jsonText != ''){
           var parsed = JSON.parse(jsonText);
           var maxX = board.width;
           var maxY = board.height;
           context.fillStyle="#808080";
           context.fillRect(0, 0, context.canvas.width, context.canvas.height);
           context.beginPath();
           for (var i = 0; i < parsed.length ; i++){
               var x = parsed[i].x*maxX;
               var y = parsed[i].y*maxY;
               if (parsed[i].type == "START"){
                   context.moveTo(x, y);
               }
               if (parsed[i].type == "MOVE"){
                   context.lineTo(x, y);
               }
           }
           context.stroke();
           context.font = "15px Arial";
           context.fillStyle="#ffffff";
           context.fillText('Board id: <%= request.getParameter("boardId") %> Watcher mode',20,20);
       }

       setTimeout(loadFromServer, 250);
    }

</script>

</html>