<!DOCTYPE html>
<html>
<%
final int MAX_LENGTH = 10;
String clientId = "noId";
clientId = session.getId();
if (clientId.length() > MAX_LENGTH){
    clientId = clientId.substring(0, MAX_LENGTH);
}
%>

<canvas height=400 width=600 id="board"></canvas>
<script type="text/javascript">
    var paint;
    var board = document.getElementById("board");
	var context = board.getContext("2d");
	context.strokeStyle = "#ffffff";

	boardrect = board.getBoundingClientRect();

    loadFromServer();

	addEventListener('mousemove', onMove, false);
	addEventListener('mousedown', onDown, false);
	addEventListener('mouseup', onUp, false);
	addEventListener('mouseleave', onUp, false);

	var clickX = new Array();
	var clickY = new Array();
	var clickDrag = new Array();

	var commands = [];
	var lastStart = 0;

	function redraw(){
	  context.lineWidth = 1;

	  for(var i=lastStart; i < clickX.length; i++) {
	    context.beginPath();

	    if (clickDrag[i] && i){
	    	context.moveTo(clickX[i-1], clickY[i-1]);

	    }
	    else{
	    	context.moveTo(clickX[i]-1, clickY[i]);
	    }
	    context.lineTo(clickX[i], clickY[i]);

	    context.closePath();
	    context.stroke();
	  }
	}

	function addClick(x, y, dragging){
		clickX.push(x);
		clickY.push(y);
		if (dragging){
			commands.push({clientId: "<%=clientId%>", x : x/context.canvas.width, y: y/context.canvas.height, type: "MOVE"});
		}
		else{
			commands.push({clientId: "<%=clientId%>", x : x/context.canvas.width, y : y/context.canvas.height, type: "START"});
		}
		clickDrag.push(dragging);
	}

	function onMove(e){
		if(paint){
			var x = e.pageX - boardrect.left;
			var y = e.pageY - boardrect.top;
			var thresshold = 2;
			if (Math.abs(clickX[clickX.length-1]-x) > thresshold || Math.abs(clickY[clickY.length-1]-y) > thresshold){
	    		addClick(x, y, true);
	    		redraw();
    		}
  		}
	}
	function onDown(e){
	  var mouseX = e.pageX - boardrect.left;
	  var mouseY = e.pageY - boardrect.top;

	  paint = true;

	  addClick(mouseX, mouseY, false);
	  redraw();
	}

	function onUp(e){
		var json = JSON.stringify(commands.slice(lastStart));
		console.log(json);
		lastStart = commands.length;
		var xhr = new XMLHttpRequest();
		xhr.open("POST", '/board/api/<%= request.getParameter("boardId") %>', true)
		xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
		xhr.send(json);

		paint = false;
	}
	function loadFromServer(){
	    if (!paint){
           var xhr = new XMLHttpRequest();
           xhr.open('GET', '/board/api/<%= request.getParameter("boardId") %>', false);
           xhr.send();
           var jsonText = xhr.responseText;
           if (jsonText != ''){
               var parsed = JSON.parse(jsonText);
               var maxX = board.width;
               var maxY = board.height;
               context.fillStyle="#808080";
               context.fillRect(0, 0, context.canvas.width, context.canvas.height);
               context.beginPath();
               for (var i = 0; i < parsed.length ; i++){
                   var x = parsed[i].x*maxX;
                   var y = parsed[i].y*maxY;
                   if (parsed[i].type == "START"){
                       context.moveTo(x, y);
                   }
                   if (parsed[i].type == "MOVE"){
                       context.lineTo(x, y);
                   }
               }
               context.stroke();
               context.font = "15px Arial";
               context.fillStyle="#ffffff";
               context.fillText('Board id: <%= request.getParameter("boardId") %> Editor mode',20,20);
           }
       }
       setTimeout(loadFromServer, 250);
    }

</script>

</html>