package com.lma.blackboard;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartupListener implements ServletContextListener{

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // Initialize board service
        Initializer.getInstance();
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
