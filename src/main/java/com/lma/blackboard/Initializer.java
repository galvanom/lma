package com.lma.blackboard;

import com.lma.blackboard.board.BoardService;
import com.lma.blackboard.board.DBService;
import com.lma.blackboard.board.RamService;

public class Initializer {
    private static volatile Initializer instance;
    private BoardService boardService;

    private Initializer(){
        DBConnector dbConnector = DBConnector.getInstance();
        if (dbConnector.getConnection() != null){
            boardService = new DBService(dbConnector);
        }
        else {
            boardService = new RamService();
        }
    }
    public static Initializer getInstance(){
        if (instance == null){
            synchronized (Initializer.class){
                if (instance == null){
                    instance = new Initializer();
                }
            }
        }
        return instance;
    }
    public BoardService getBoardService(){
        return boardService;
    }
}
