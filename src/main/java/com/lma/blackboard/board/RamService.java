package com.lma.blackboard.board;

import com.lma.blackboard.board.ramboards.Board;
import com.lma.blackboard.board.ramboards.SimpleBoard;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RamService implements BoardService{
    private final Set<Board> boards = Collections.newSetFromMap(new ConcurrentHashMap<Board, Boolean>());

    public RamService(){
    }
    public void addCommands(int boardId, String jsonCommands){
        Board board = getBoard(boardId);
        board.addCommands(jsonCommands);
    }
    public String getCommands(int boardId){
        Board board = findBoardById(boardId);
        if (board != null){
            return board.getCommands();
        }
        return "";
    }
    private Board getBoard(int boardId){
        Board board = findBoardById(boardId);
        if (board == null){
            synchronized (this) {   // If two threads try to create board with the same id
                Board _board =  findBoardById(boardId);
                if ( _board != null){
                    board = _board;
                }
                else {
                    // Change to "new Board(boardId)" for using Board class
                    board = new SimpleBoard(boardId);
                    boards.add(board);
                }
            }
        }
        return board;
    }
    private Board findBoardById(int id){
        for (Board board : boards){
            if (board.getId() == id){
                return board;
            }
        }
        return null;
    }

    public int createNewBoard() {
        int max = 1;
        List<Integer> boardsId = getBoardsId();
        if (!boardsId.isEmpty()) {
            max = Collections.max(boardsId) + 1;
        }
        getBoard(max);
        return max;
    }

    public List<Integer> getBoardsId() {
        List<Integer> boardsId = new LinkedList<Integer>();
        for (Board board : boards){
            boardsId.add(board.getId());
        }
        return boardsId;
    }
}
