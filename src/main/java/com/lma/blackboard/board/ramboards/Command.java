package com.lma.blackboard.board.ramboards;

public class Command {
    private final String jsonString;
    private final String clientId;
    private final String type;
    private final double x;
    private final double y;

    private Command(String jsonString,
                    String clientId,
                    String type,
                    double x,
                    double y){
        this.jsonString = jsonString;
        this.clientId = clientId;
        this.type = type;
        this.x = x;
        this.y = y;
    }
    public String getJsonString() {
        return jsonString;
    }
    public String getClientId() {
        return clientId;
    }
    public String getType() {
        return type;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public static class Builder{
        private String jsonString;
        private String clientId;
        private String type;
        private double x;
        private double y;

        public Builder setJsonString(String jsonString) {
            this.jsonString = jsonString;
            return this;
        }
        public Builder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }
        public Builder setType(String type) {
            this.type = type;
            return this;
        }
        public Builder setX(double x) {
            this.x = x;
            return this;
        }
        public Builder setY(double y) {
            this.y = y;
            return this;
        }
        public Command build(){
            return new Command(jsonString,clientId,type,x,y);
        }
    }
    @Override
    public String toString() {
        return jsonString;
    }
}
