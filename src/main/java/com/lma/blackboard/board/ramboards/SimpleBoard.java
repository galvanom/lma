package com.lma.blackboard.board.ramboards;


import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SimpleBoard extends Board {
    private final List<Map<String,Object>> commands = new JSONArray();
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    public SimpleBoard(int id){
        super(id);
    }

    @Override
    public void addCommands(String jsonString){
        JSONParser parser = new JSONParser();
        try {
            JSONArray list = (JSONArray)parser.parse(jsonString);
            if (checkFields(list)) {
                writeLock.lock();
                commands.addAll(list);
                writeLock.unlock();
            }
        }
        catch (ParseException e){
            System.out.println("Not valid JSON object");
        }
        catch (ClassCastException e){
            System.out.println("Not valid JSON object class");
        }
    }
    @Override
    public String getCommands(){
        readLock.lock();
        String returnStatement = ((JSONArray)commands).toJSONString();
        readLock.unlock();
        return  returnStatement;
    }
    private boolean checkFields(JSONArray list){
        boolean valid = false;
        try {
            for (Object object : list) {
                Map<String,Object> command = (Map<String,Object>)object;

                if (    !command.containsKey("clientId") ||
                        !command.containsKey("type") ||
                        !command.containsKey("x") ||
                        !command.containsKey("y")){
                    return false;
                }
            }
            valid = true;
        }
        catch (ClassCastException e){
            System.out.println("Not valid JSON object class");
        }
        return valid;
    }
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
