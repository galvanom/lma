package com.lma.blackboard.board.ramboards;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class Board {
    private final int id;
    private final List<Command> commands = new CopyOnWriteArrayList<Command>();
    private final List<Map<String, Object>> cmds = new LinkedList<Map<String, Object>>();

    Board(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }

    public void addCommands(String jsonString){

        commands.addAll(parseCommands(jsonString));
    }
    public String getCommands(){
        StringBuilder jsonArray = new StringBuilder();
        jsonArray.append("[");

        Iterator<Command> it = commands.iterator();
        int commandsNumber = commands.size();
        int i = 0;
        while(it.hasNext()) {
            jsonArray.append(it.next());
            if (i++ < commandsNumber - 1) {
                jsonArray.append(",");
            }
        }
        jsonArray.append("]");
        return jsonArray.toString();
    }
    private List<Command> parseCommands(String jsonString){
        List<Command> commands = new LinkedList<Command>();
        JSONParser parser = new JSONParser();

        try {
            JSONArray jsonCommands = (JSONArray) parser.parse(jsonString);
            for (int i = 0; i < jsonCommands.size(); i++) {
                Command command = parseCommand((JSONObject)jsonCommands.get(i));
                if (command != null){
                    commands.add(command);
                }
            }
        }
        catch (ParseException e){
            System.out.println("Can't parse commands");
            e.printStackTrace();
        }
        catch (ClassCastException e){
            System.out.println("Can't parse commands");
            e.printStackTrace();
        }
        return commands;
    }
    private Command parseCommand(JSONObject jsonCommand){
        Command command = null;
        String type;
        String clientId;
        double x;
        double y;
        if (jsonCommand.containsKey("clientId")
                && jsonCommand.containsKey("type")
                && jsonCommand.containsKey("x")
                && jsonCommand.containsKey("y")) {
            try {

                clientId = (String) jsonCommand.get("clientId");
                type = (String) jsonCommand.get("type");
                x = (Double) jsonCommand.get("x");
                y = (Double) jsonCommand.get("y");
                command = new Command.Builder()
                        .setJsonString(jsonCommand.toJSONString())
                        .setClientId(clientId)
                        .setType(type)
                        .setX(x)
                        .setY(y)
                        .build();

                System.out.printf("%s\n%s\n%s\n%.8f\n%.8f\n", command.getJsonString(),
                        command.getClientId(), command.getType(), command.getX(), command.getY());
            } catch (ClassCastException e) {
                System.out.println("Bad command format\n");
                e.printStackTrace();
            }
        }

        return command;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o != null){
            if (o.getClass() == this.getClass()){
                Board board = (Board)o;
                if (board.getId() == id){
                    return true;
                }
            }
        }
        return false;
    }
}
