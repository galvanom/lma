package com.lma.blackboard.board;

import com.lma.blackboard.DBConnector;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class DBService implements BoardService {
    private final DBConnector dbConnector;
    private final Connection db;
    private final AtomicInteger commandsUniqueId = new AtomicInteger(0);
    private final static String NEW_BOARD_CLIENT_ID = "NEWBOARD";

    public DBService(DBConnector connector){
        dbConnector = connector;
        db = dbConnector.getConnection();
        Statement statement = null;
        try {
            statement = dbConnector.getConnection().createStatement();
            ResultSet rs = statement.executeQuery("SELECT MAX(cid) AS maxid FROM commands");
            while (rs.next()){
                commandsUniqueId.set(rs.getInt("maxid"));
            }
        }
        catch (SQLException e){
            System.out.println("Error while quering maximal command table id " + e);
        }
        catch (Exception e){
            System.out.println("Error while calculating maximal command table id " + e);
        }
        finally {
            try {
                statement.close();
            }
            catch (SQLException e){
                System.out.println("Can't close DB connection " + e);
            }
        }
    }
    public void addCommands(int boardId, String jsonCommands){
        JSONParser parser = new JSONParser();
        Statement statement = null;
        try {
            JSONArray list = (JSONArray) parser.parse(jsonCommands);
            statement = db.createStatement();
            for (Object object : list){
                statement.addBatch(insertCommand(boardId, (Map<String,Object>)object));
            }
            synchronized (this) {
                statement.executeBatch();
            }
        }
        catch(ParseException e){
            System.out.println("Not valid JSON " + e);
        }
        catch (ClassCastException e){
            System.out.println("Not valid JSON Object " + e);
        }
        catch (SQLException e){
            System.out.println("Error while inserting JSON data " + e);
        }
        finally {
            try{
                if (statement != null) {
                    statement.close();
                }
            }
            catch(SQLException e){
                System.out.println("Can't close DB connection " + e);
            }
        }
    }
    public String getCommands(int boardId) {
        JSONArray commands = new JSONArray();
        Statement statement = null;
        try{
            statement = db.createStatement();
            ResultSet rs = statement.executeQuery("SELECT clientId, ctype, x, y FROM Commands WHERE boardId = " +
                    boardId + " ORDER BY cid");
            while(rs.next()){
                Map<String,Object> command = new HashMap<String, Object>();
                command.put("clientId", rs.getObject("clientId"));
                command.put("type", rs.getObject("ctype"));
                command.put("x", rs.getObject("x"));
                command.put("y", rs.getObject("y"));
                if (!command.get("clientId").equals(NEW_BOARD_CLIENT_ID)) {
                    commands.add(command);
                }
            }
        }
        catch (SQLException e){
            System.out.println("Error while quering commands " + e);
        }
        finally {
            try{
                if (statement != null) {
                    statement.close();
                }
            }
            catch(SQLException e){
                System.out.println("Can't close DB connection " + e);
            }
        }
        return commands.toJSONString();
    }

    private String insertCommand(int boardId, Map<String, Object> command){
        StringBuilder insert = new StringBuilder();
        try {
            if (    command.containsKey("clientId") &&
                    command.containsKey("type") &&
                    command.containsKey("x")&&
                    command.containsKey("y")) {

                insert.append("insert into commands (cid, boardId, clientId, ctype, x, y)");
                insert.append("VALUES(");
                insert.append(commandsUniqueId.incrementAndGet()).append(",");
                insert.append(boardId).append(",");
                insert.append("'" + command.get("clientId") + "'").append(",");
                insert.append("'" + command.get("type") + "'").append(",");
                insert.append(command.get("x")).append(",");
                insert.append(command.get("y"));
                insert.append(")");
            }
            else{
                System.out.println("Additional field is requeried");
            }
        }
        catch (Exception e){
            System.out.println("Error while constructing commands insert query " + e);
        }
        return insert.toString();
    }

    public synchronized List<Integer> getBoardsId() {
        List<Integer> ids = new LinkedList<Integer>();
        Statement statement = null;
        try{
            statement = db.createStatement();
            ResultSet rs = statement.executeQuery("SELECT DISTINCT boardid from commands ORDER BY boardid");
            while (rs.next()){
                ids.add(rs.getInt("boardid"));
            }
        }
        catch (SQLException e){
            System.out.println("Can't get boards ids " + e);
        }
        finally {
            try{
                if (statement != null) {
                    statement.close();
                }
            }
            catch(SQLException e){
                System.out.println("Can't close DB connection " + e);
            }
        }
        return ids;
    }

    public int createNewBoard() {
        int newBoardId = genNewBoardId();
        Statement statement = null;
        try{
            statement = db.createStatement();
            statement.execute("insert into commands (cid, boardId, clientId, ctype, x, y)"
                    + "values(" + commandsUniqueId.incrementAndGet()+"," + newBoardId+", '"
                    + NEW_BOARD_CLIENT_ID  + "', 'MOVE', 0.0, 0.0)");
        }
        catch (SQLException e){
            System.out.println("Can't create new board " + e);
        }
        finally {
            try{
                if (statement != null) {
                    statement.close();
                }
            }
            catch(SQLException e){
                System.out.println("Can't close DB connection " + e);
            }
        }

        return  newBoardId;
    }
    private int genNewBoardId(){
        List<Integer> boardsList = getBoardsId();
        if (boardsList.isEmpty()){
            return 1;
        }
        return Collections.max(boardsList)+1;
    }
}
