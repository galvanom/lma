package com.lma.blackboard.board;

import java.util.List;

public interface BoardService {
    void addCommands(int boardId, String jsonCommands);
    String getCommands(int boardId);
    List<Integer> getBoardsId();
    int createNewBoard();
}
