package com.lma.blackboard;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConnector {
    private static volatile DBConnector dbConnector;
    private final String DATASOURCE_CONTEXT = "java:/boardDS";
    private final Connection connection;

    private DBConnector(){
        connection = getJNDIConnection();
        if (connection != null) {
            try {
                Statement statement = connection.createStatement();
                statement.execute("CREATE TABLE Commands(" +
                        "cid int NOT NULL," +
                        "boardId int NOT NULL," +
                        "clientId varchar(255) NOT NULL," +
                        "ctype varchar(20) NOT NULL," +
                        "x real NOT NULL," +
                        "y real NOT NULL," +
                        "CONSTRAINT commandID PRIMARY KEY (cid) )");
                statement.close();

            } catch (SQLException e) {
                System.out.println("Can't create table " + e);
            }
        }

    }
    public static DBConnector getInstance() {
        if (dbConnector == null){
            synchronized (DBConnector.class){
                if (dbConnector == null){
                    dbConnector = new DBConnector();
                }
            }
        }
        return dbConnector;
    }
    public Connection getConnection(){
        return connection;
    }

    Connection getJNDIConnection(){

        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null){
                System.out.println("JNDI problem. Cannot get InitialContext.");
            }
            DataSource datasource = (DataSource)initialContext.lookup(DATASOURCE_CONTEXT);
            if (datasource != null) {
                result = datasource.getConnection();
            }
            else {
                System.out.println("Failed to lookup datasource.");
            }
        }
        catch (NamingException e) {
            System.out.println("Cannot get connection: " + e);
        }
        catch(SQLException e){
            System.out.println("Cannot get connection: " + e);
        }
        return result;
    }


}
