package com.lma.blackboard.view;

import com.lma.blackboard.board.BoardService;
import com.lma.blackboard.Initializer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class IndexServlet extends HttpServlet{
    private final BoardService boardService = Initializer.getInstance().getBoardService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List boards = boardService.getBoardsId();
        req.setAttribute("boards", boards);
        req.getRequestDispatcher("main.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int newBoardId = boardService.createNewBoard();
        System.out.println(newBoardId);
        List boards = boardService.getBoardsId();
        req.setAttribute("boards", boards);
        req.getRequestDispatcher("main.jsp").forward(req, resp);
    }
}
