package com.lma.blackboard.view;

import com.lma.blackboard.board.BoardService;
import com.lma.blackboard.Initializer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class APIServlet extends HttpServlet {
    private final BoardService boardService = Initializer.getInstance().getBoardService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        String path = req.getPathInfo();
        if (path != null) {
            int boardId = getBoardId(path);
            if (boardId != -1) {
                out.printf("%s", boardService.getCommands(boardId));
            }
        }
    }
//    curl -H "Content-Type: application/json" -X POST -d '[{"clientId": "60:21:C0:2A:E0:33", "type": "START", "x": 0.15703125,"y": 0.28644067}]'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        if (req.getContentType().contains("application/json")) {
            String path = req.getPathInfo();
            if (path != null) {
                int boardId = getBoardId(req.getPathInfo());
                if (boardId != -1) {
                    System.out.println("Adding move to the board");
                    StringBuffer jsonCommands = new StringBuffer();
                    String line;
                    BufferedReader reader = req.getReader();
                    while ((line = reader.readLine()) != null) {
                        jsonCommands.append(line);
                    }
                    boardService.addCommands(boardId, jsonCommands.toString());
                    writer.printf("Board id: %d JSON: %s\n", boardId, jsonCommands);
                }
                else{
                    System.out.println("Can't parse board id");
                }
            }
            else{
                System.out.println("Wrong path");
            }
        }
        else{
            System.out.printf("Not JSON content type: %s\n", req.getContentType() );
        }
    }
    /**
     * Returns board identifier form the path
     * @param pathInfo
     * @return board identifier or -1
     */
    public static int getBoardId(String pathInfo){
        int boardId = -1;
        if (pathInfo.startsWith("/") && pathInfo.length() > 1){
            try {
                boardId = Integer.parseInt(pathInfo.substring(1));
            }
            catch (NumberFormatException e){
                System.out.println("Board id is not valid");
            }
        }
        return boardId;
    }

}
